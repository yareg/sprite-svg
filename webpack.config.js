var webpack = require('webpack');

module.exports = {
    context: __dirname,
    entry: "./index.js",
    output: {
        filename: "bundle.js"
    },
    plugins: [
    ]    ,
    module: {
        rules: [
            {
                test: /\.svg$/,
                loader: 'svg-sprite-loader',
                options: {esModule:false}
            },
        ]
    }
};