module.exports = {
    doAction: function() {
        var text = 'bla-bla-bla';
        console.log(text);
        document.getElementById('label').innerHTML = text;

        // uncommenting line below will cause error - 'import' and 'export' may only appear at the top level
        var menuAbout = require( './appMenu-About.svg');

        console.log(menuAbout);

        var svgElement = document.getElementById('svg1');
        svgElement.setAttribute('viewBox', menuAbout.viewBox);

        svgElement.getElementsByTagName('use')[0].setAttribute('xlink:href', '#' + menuAbout.id);
    }
};